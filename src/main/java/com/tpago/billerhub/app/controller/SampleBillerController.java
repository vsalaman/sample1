package com.tpago.billerhub.app.controller;

import com.tpago.billerhub.app.dto.Biller;
import com.tpago.billerhub.app.data.Repo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@SuppressWarnings("Duplicates")
@Slf4j
@RestController
public class SampleBillerController
{
    @Autowired
    Repo repo;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Resources<Resource>> index()
    {
        List<Link> links=new ArrayList<Link>();
        links.add(linkTo(SampleBillerController.class).slash("/").withSelfRel());
        links.add(linkTo(SampleBillerController.class).slash("/billers").withRel("billers"));
        List<Resource> resources = new ArrayList<Resource>();
        Resources<Resource> resourceList = new Resources<Resource>(resources, links);

        return new ResponseEntity<Resources<Resource>>(resourceList, HttpStatus.OK);
    }

    @RequestMapping(value = "/billers", method = RequestMethod.GET)
    public ResponseEntity<Resources<Resource>> billersRoot()
    {
        List<Link> links=new ArrayList<Link>();
        links.add(linkTo(SampleBillerController.class).slash("/billers").withSelfRel());

        List<Resource> resources = new ArrayList<Resource>();
        List<Biller> billers = repo.getBillers();
        for (Biller biller : billers)
        {
            Link billerLink = linkTo(SampleBillerController.class)
                .slash("/billers")
                .slash(biller.getCountry())
                .slash(biller.getBillerId())
                .withSelfRel();
            Resource<Biller> r=new Resource<Biller>(biller,billerLink);
            resources.add(r);
        }

        Resources<Resource> resourceList = new Resources<Resource>(resources, links);
        return new ResponseEntity<Resources<Resource>>(resourceList, HttpStatus.OK);
    }

    @RequestMapping(value = "/billers/{country}/{billerId}", method = RequestMethod.GET)
    public ResponseEntity<Resources<Resource>> biller(
        @PathVariable("billerId") String billerId,
        @PathVariable("country") String country)
    {
        List<Link> links=new ArrayList<Link>();
        links.add(linkTo(SampleBillerController.class)
                      .slash("/billers")
                      .slash(country)
                      .slash(billerId)
                      .withSelfRel());

        List<Resource> resources = new ArrayList<Resource>();
        List<Biller> billers = repo.getBillers();
        for (Biller biller : billers)
        {
            if(biller.getCountry().equals(country) && biller.getBillerId().equals(billerId))
            {
                Resource<Biller> r=new Resource<Biller>(biller);
                resources.add(r);
                break;
            }
        }

        Resources<Resource> resourceList = new Resources<Resource>(resources, links);
        return new ResponseEntity<Resources<Resource>>(resourceList, HttpStatus.OK);
    }
}
