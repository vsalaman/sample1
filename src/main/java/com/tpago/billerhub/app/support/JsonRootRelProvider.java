package com.tpago.billerhub.app.support;

import com.fasterxml.jackson.annotation.JsonRootName;
import org.atteo.evo.inflector.English;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.core.DefaultRelProvider;

public class JsonRootRelProvider implements RelProvider
{
    DefaultRelProvider defaultRelProvider = new DefaultRelProvider();

    @Override
    public String getItemResourceRelFor(Class<?> type)
    {
        JsonRootName rootName = getRootName(type);
        return rootName != null ? rootName.value() : defaultRelProvider.getItemResourceRelFor(type);
    }

    @Override
    public String getCollectionResourceRelFor(Class<?> type)
    {
        JsonRootName rootName = getRootName(type);
        return rootName != null ? English.plural(rootName.value()) :
               English.plural(defaultRelProvider.getItemResourceRelFor(type));
    }

    @Override
    public boolean supports(Class<?> delimiter)
    {
        return defaultRelProvider.supports(delimiter);
    }

    private JsonRootName getRootName(Class<?> type)
    {
        JsonRootName[] annotations = type.getAnnotationsByType(JsonRootName.class);
        return annotations != null && annotations.length > 0 ? annotations[0] : null;
    }
}
