package com.tpago.billerhub.app.config;

import com.tpago.billerhub.app.controller.SampleBillerController;
import com.tpago.billerhub.app.data.Repo;
import com.tpago.billerhub.app.dto.Biller;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;

import java.util.ArrayList;

@Configuration
public class AppCfg
{
    @Bean
    public Repo getRepo()
    {
        Repo repo = new Repo();
        ArrayList<Biller> billers = new ArrayList<Biller>();
        billers.add(new Biller("BILLER1", "DO", "Biller 1"));
        billers.add(new Biller("BILLER2", "DO", "Biller 2"));
        billers.add(new Biller("BILLER3", "DO", "Biller 3"));
        billers.add(new Biller("BILLER4", "US", "Biller 4"));
        billers.add(new Biller("BILLER5", "US", "Biller 5"));
        billers.add(new Biller("BILLER6", "US", "Biller 6"));
        repo.setBillers(billers);
        return repo;
    }
}
