package com.tpago.billerhub.app.config;

import com.tpago.billerhub.app.controller.SampleBillerController;
import com.tpago.billerhub.app.support.JsonRootRelProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@EnableHypermediaSupport(type= HypermediaType.HAL)
public class WebMvcCfg extends WebMvcConfigurerAdapter
{
    @Bean
    public SampleBillerController getBillerController()
    {
        return new SampleBillerController();
    }

    @Bean
    public RelProvider relProvider()
    {
        return new JsonRootRelProvider();
    }
}
