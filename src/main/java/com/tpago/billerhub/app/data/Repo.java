package com.tpago.billerhub.app.data;

import com.tpago.billerhub.app.dto.Biller;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Repo
{
    List<Biller> billers;
}
