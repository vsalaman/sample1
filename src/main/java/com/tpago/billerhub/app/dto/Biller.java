package com.tpago.billerhub.app.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;

@Data
@JsonRootName("biller")
public class Biller
{
    @NonNull
    String billerId;

    @NonNull
    String country;

    @NonNull
    String name;
}
