package com.tpago.billerhub.app.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;

@Data
@JsonRootName("country")
public class Country
{
    @NonNull
    String name;
}
